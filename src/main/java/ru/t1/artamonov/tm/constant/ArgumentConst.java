package ru.t1.artamonov.tm.constant;

public final class ArgumentConst {

    public final static String ARG_HELP = "-h";

    public final static String ARG_VERSION = "-v";

    public final static String ARG_ABOUT = "-a";

    public final static String ARG_INFO = "-i";

}
